package com.sda.service;

public interface RateService {

    void addRateService(long userId, String rate);

    Long getRateIdByName(String rateName);
}
