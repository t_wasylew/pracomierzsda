package com.sda.service;

import com.sda.model.User;

public interface UserService {
    User getUserWithID(long id);
    boolean registerUser(String login, String password);
    boolean userExists(String login);
    long getUserID();
    Long login(String login, String pass);
}
