package com.sda.service;

public class Services {

    private static final Services INSTANCE = new Services();

    private final UserService userService = new UserServiceImpl();
    private final WorkLogService workLogService = new WorkLogServiceImpl();
    private final RateService rateService = new RateServiceImpl();

    private Services() {
    }

    public static Services getInstance(){
        return INSTANCE;
    }

    public UserService getUserService() {
        return userService;
    }

    public WorkLogService getWorkLogService() {
        return workLogService;
    }

    public RateService getRateService() {
        return rateService;
    }
}
