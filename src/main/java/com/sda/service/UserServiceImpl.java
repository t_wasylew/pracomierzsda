package com.sda.service;

import com.sda.model.User;
import org.apache.commons.codec.digest.DigestUtils;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

public class UserServiceImpl implements UserService {

    private static long USER_ID_COUNTER = 1;
    private Map<Long, User> userMap = new HashMap<>();

    public User getUserWithID(long id) {
        return userMap.get(id);
    }

    public boolean registerUser(String login, String password) {
        if (userExists(login)) {
            System.out.println("This login is already taken.");
            return false;
        }
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            byte[] encrypted = digest.digest(password.getBytes());
            String hashed = DigestUtils.md5Hex(encrypted).toLowerCase();
            System.out.println("Your login: " + login);
            System.out.println("Encrypted password is: " + hashed);

            User newUser = new User(USER_ID_COUNTER++, login, hashed);
            userMap.put(newUser.getId(), newUser);
            return true;
        } catch (NoSuchAlgorithmException e) {
            System.err.println("Unable to encrypt password - no such algorithm");
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean userExists(String login) {
        for (Map.Entry<Long, User> entry : userMap.entrySet()) {
            if (entry.getValue().getLogin().equalsIgnoreCase(login)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public long getUserID() {
        return USER_ID_COUNTER;
    }

    @Override
    public Long login(String login, String pass) {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        byte[] encrypted = digest.digest(pass.getBytes());
        String hashed = DigestUtils.md5Hex(encrypted).toLowerCase();
        for (Map.Entry<Long, User> entry : userMap.entrySet()) {
            if (entry.getValue().getLogin().equals(login) && entry.getValue().getPssswordHash().equals(hashed)) {
                System.out.println("User logged");
                return entry.getKey();
            }
        }
        return null;
    }


}
