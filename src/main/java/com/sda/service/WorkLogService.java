package com.sda.service;

public interface WorkLogService {
    boolean addWorkLog(String day, String workStart, String workEnd, long userID, long rateID);
    void printWorkLog(String day, long userID);
}
