package com.sda.service;


import com.sda.model.WorkLog;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

public class WorkLogServiceImpl implements WorkLogService {

    private static long WORK_LOG_ID_COUNTER = 1;

    private Map<LocalDate, List<WorkLog>> workLogMap = new HashMap<>();


    @Override
    public boolean addWorkLog(String day, String workStart, String workEnd, long userID, long rateID) {

        WorkLog newWorkLog = new WorkLog(
                LocalDate.parse(day),
                LocalTime.parse(workStart),
                LocalTime.parse(workEnd),
                userID,
                WORK_LOG_ID_COUNTER++,
                rateID);

        if (workLogMap.containsKey(LocalDate.parse(day))){
            workLogMap.get(LocalDate.parse(day)).add(newWorkLog);
            return true;
        }else{
            workLogMap.put(LocalDate.parse(day), new ArrayList<>());
            workLogMap.get(LocalDate.parse(day)).add(newWorkLog);
            return true;
        }
    }

    @Override
    public void printWorkLog(String day, long userID) {
        for (int i = 0; i <workLogMap.get(LocalDate.parse(day)).size() ; i++) {
            if(workLogMap.get(LocalDate.parse(day)).get(i).getUserID() == userID){
                System.out.println(workLogMap.get(LocalDate.parse(day)).get(i).toString());

            }else System.out.println("Empty log");
        }
    }
}
