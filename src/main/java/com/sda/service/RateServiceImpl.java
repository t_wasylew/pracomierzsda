package com.sda.service;


import com.sda.model.Rate;

import java.util.HashMap;
import java.util.Map;

public class RateServiceImpl implements RateService{
    private static long RATE_ID_COUNTER = 1;
    private Map<Long,Rate> rateMap = new HashMap<>();

    @Override
    public void addRateService(long userId, String rate) {
        Rate newRate = new Rate(RATE_ID_COUNTER++, userId, rate);
        rateMap.put(newRate.getRateId(), newRate);
    }

    @Override
    public Long getRateIdByName(String rateName) {
        return null;
    }
}
