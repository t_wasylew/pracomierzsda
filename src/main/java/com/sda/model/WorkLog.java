package com.sda.model;

import java.time.LocalDate;
import java.time.LocalTime;

public class WorkLog {
    private LocalTime workStartsAt;
    private LocalTime workEndsAt;
    private LocalDate day;
    private long userID, rateID, workLogID;


    @Override
    public String toString() {
        return "Day: " +day+ " Work start: " +workStartsAt+ " Work end: "+workEndsAt ;
    }

    public WorkLog(LocalDate day, LocalTime workStartsAt, LocalTime workEndsAt, long userID, long workLogID , long rateID) {
        this.workStartsAt = workStartsAt;
        this.workEndsAt = workEndsAt;
        this.day = day;
        this.userID = userID;
        this.workLogID = workLogID;
        this.rateID = rateID;
    }

    public LocalTime getWorkStartsAt() {
        return workStartsAt;
    }

    public void setWorkStartsAt(LocalTime workStartsAt) {
        this.workStartsAt = workStartsAt;
    }

    public LocalTime getWorkEndsAt() {
        return workEndsAt;
    }

    public void setWorkEndsAt(LocalTime workEndsAt) {
        this.workEndsAt = workEndsAt;
    }

    public LocalDate getDay() {
        return day;
    }

    public void setDay(LocalDate day) {
        this.day = day;
    }

    public long getUserID() {
        return userID;
    }

    public void setUserID(long userID) {
        this.userID = userID;
    }

    public long getRateID() {
        return rateID;
    }

    public void setRateID(long rateID) {
        this.rateID = rateID;
    }

    public long getWorkLogID() {
        return workLogID;
    }

    public void setWorkLogID(long workLogID) {
        this.workLogID = workLogID;
    }
}
