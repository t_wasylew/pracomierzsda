package com.sda.model;

public class Rate {

    private long rateId;
    private long userId;
    private String workRate;


    public Rate(long rateId, long userId, String workRate) {
        this.rateId = rateId;
        this.userId = userId;
        this.workRate = workRate;
    }

    public Rate() {
    }

    public long getRateId() {
        return rateId;
    }

    public void setRateId(long rateId) {
        this.rateId = rateId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getWorkRate() {
        return workRate;
    }

    public void setWorkRate(String workRate) {
        this.workRate = workRate;
    }
}
