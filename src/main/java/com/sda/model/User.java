package com.sda.model;

public class User {

    private long id;
    private String name;
    private String login;
    private String pssswordHash;
    private Long defaultRateId;

    public User() {
    }


    public User(long id, String login, String pssswordHash) {
        this.id = id;
        this.login = login;
        this.pssswordHash = pssswordHash;
    }

    public User(long id, String login, String pssswordHash, Long hourRate) {
        this.id = id;
        this.login = login;
        this.pssswordHash = pssswordHash;
        this.defaultRateId = hourRate;
    }

    public Long getDefaultRateId() {
        return defaultRateId;
    }

    public void setDefaultRateId(Long defaultRateId) {
        this.defaultRateId = defaultRateId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPssswordHash() {
        return pssswordHash;
    }

    public void setPssswordHash(String pssswordHash) {
        this.pssswordHash = pssswordHash;
    }
}
