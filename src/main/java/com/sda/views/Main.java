package com.sda.views;

import java.util.Scanner;

public class Main {
    private static Scanner sc = new Scanner(System.in);
    private static CommandParser parser = new CommandParser();

    public static void main(String[] args) {
        System.out.println("Hello dear user!");
        while (sc.hasNextLine()) {
            System.out.println("Main menu, commands: \n" +
                    "1. register new user \n" +
                    "2. login/logout"+
                    "3. quit \n");

//            System.out.println("Commands: \n" +
//                    "1. user register: login, password and hour rate \n" +
//                    "2. user login: login and password\n" +
//                    "4. user logout \n"+
//                    "5. add work log: date(yyyy-MM-dd), work start time (HH:mm), work stop time(HH:mm) \n" +
//                    "6. print: date(yyyy-MM-dd)\n" +
//                    "7. type quit to close app\n");
            String line = sc.nextLine();
            if (line.equalsIgnoreCase("quit")) {
                break;
            }else if(line.startsWith("register new user")){
                registerNewUserMenu();
            }else if(line.startsWith("login")){
                loginLogoutUserMenu();
            }
//            } else if (line.trim().toLowerCase().startsWith("add work log") || line.trim().toLowerCase().startsWith("print")) {
//                parser.parseLineWork(line);
//            } else {
//                parser.parseLine(line);
//            }
        }
    }

    private static void registerNewUserMenu(){
        while (sc.hasNextLine()){
            System.out.println("Register new user, commands:\n"+
            "1. To register new user type: user register: add user: login password \n"+
            "2. Type quit to return to main menu\n");
            String line = sc.nextLine();
            if (line.equalsIgnoreCase("quit")) {
                break;
            } else if (line.trim().toLowerCase().startsWith("add user:")) {
                parser.parseRegisterUser(line);
                System.out.println("Please write your default rate: ");
                String line2 = sc.nextLine();
                parser.parseDefaultRateConfig(line2);
            }
        }
    }

    private static void loginLogoutUserMenu(){
        while (sc.hasNextLine()){
            System.out.println("Work log menu, commands:\n"+
            "1. login: login password\n"+
            "2. logout\n" +
            "3. Type quit to return to main menu\n");
            String line = sc.nextLine();
            if (line.equalsIgnoreCase("quit")) {
                break;
            } else if (line.trim().toLowerCase().startsWith("add work log") || line.trim().toLowerCase().startsWith("print")) {
                parser.parseLineWork(line);
            } else {
                parser.parseLine(line);
            }
        }
    }
}



