package com.sda.views;

import com.sda.controller.RateController;
import com.sda.controller.UserController;
import com.sda.controller.WorkLogController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Optional;

public class CommandParser {
    private UserController controller = new UserController();
    private WorkLogController workLogController = new WorkLogController();
    private SessionCookies cookies = new SessionCookies();
    private RateController rateController = new RateController();


    private void parseUserLoginCommand(String line) {
        String[] words = line.trim().split(" ");
        if (words[1].equalsIgnoreCase("login")) {
            // jeśli nie jesteśmy zalogowani
            if (!cookies.isLoggedIn()) {
                // próbujemy sie zalogować
                Optional<Long> longOptional = controller.login(words[2], words[3]);
                if (longOptional.isPresent()) {
                    // zalogowani
                    cookies.setLoggedIn(true);
                    cookies.setLoggedUserId(longOptional.get());
                } else {
                    // nieudana próba logowania
                    cookies.setLoggedIn(false);
                    System.out.println("Login error");
                }
            } else {
                System.out.println("Someone is logged in");
            }
//        } else if (words[1].equalsIgnoreCase("logout")) {
//            cookies.setLoggedIn(false);
//            System.out.println("Logout finished");
        }
    }

    public void parseLineWork(String line) {
        String[] words = line.trim().split(" ");
        if (words[0].equals("print")) {
            workLogController.printWorkLog(words[1], cookies.getLoggedUserId());
        } else if (isValidDate(words[3]) && isValidTime(words[4]) && isValidTime(words[5])) {
            workLogController.addWorkLogWithDefaultRateId(words[3], words[4], words[5], cookies.getLoggedUserId());
        }

    }

    public void parseDefaultRateConfig(String line2) {
        String[] words = line2.trim().split(" ");
        rateController.addRateService(cookies.getLoggedUserId(), words[0]);
    }

    public void parseRegisterUser(String line) {
        String[] words = line.trim().split(" ");
        controller.registerUser(words[2], words[3]);
    }

    private boolean isValidTime(String word) {
        if (word == null || !word.matches("(^([0-1]?\\d|2[0-3]):([0-5]?\\d):([0-5]?\\d)$)|(^([0-5]?\\d):([0-5]?\\d)$)")) {
            System.out.println("Wrong time format");
            return false;
        }
        return true;
    }

    public boolean isValidDate(String text) {
        if (text == null || !text.matches("\\d{4}-[01]\\d-[0-3]\\d")) {
            System.out.println("Wrong date");
            return false;
        } else {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            df.setLenient(false);
            try {
                df.parse(text);
                return true;
            } catch (ParseException ex) {
                System.out.println("Wrong date");
                return false;
            }
        }
    }
}
