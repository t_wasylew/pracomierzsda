package com.sda.controller;

import com.sda.service.RateService;
import com.sda.service.Services;

public class RateController {

    private RateService rateService = Services.getInstance().getRateService();

    public void addRateService(long userId, String rate){
        rateService.addRateService(userId, rate);
        System.out.println("Rate added");
    }
}
