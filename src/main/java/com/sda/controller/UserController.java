package com.sda.controller;

import com.sda.service.Services;
import com.sda.service.UserService;
import com.sda.service.UserServiceImpl;

import java.util.Optional;

public class UserController {

    private UserService userService = Services.getInstance().getUserService();

    public void registerUser(String login, String password) {
        if(userService.registerUser(login, password)){
            System.out.println("Udało Ci się zarejestrować. Witamy!");
        }else{
            System.err.println("Nie udało się zalogować");
        }
    }

    public Optional<Long> login(String login, String pass) {
        return Optional.ofNullable(userService.login(login, pass));
    }
}
