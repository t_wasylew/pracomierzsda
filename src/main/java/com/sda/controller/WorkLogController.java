package com.sda.controller;

import com.sda.model.User;
import com.sda.service.*;

public class WorkLogController {
    private WorkLogService workLogService = Services.getInstance().getWorkLogService();
    private UserService userService = Services.getInstance().getUserService();
    private RateService rateService = Services.getInstance().getRateService();

    public void addWorkLogWithDefaultRateId(String day, String workStart, String workEnd, long userID){
        User u = userService.getUserWithID(userID);
        if(u.getDefaultRateId() == null){
            System.out.println("Rate is not set.");
        }
        if(workLogService.addWorkLog(day,workStart,workEnd,userID, u.getDefaultRateId())){
            System.out.println("Log added");
        }
    }

    public void addWorkLogWith(String day, String workStart, String workEnd, long userID, String rateName){
        User u = userService.getUserWithID(userID);
        if (workLogService.addWorkLog(day, workStart, workEnd, userID, rateService.getRateIdByName(rateName))) {
            System.out.println("Log added");
        }
    }

    public void printWorkLog(String day, long userID) {
        workLogService.printWorkLog(day, userID);
    }
}
